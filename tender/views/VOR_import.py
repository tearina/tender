# -*- coding: utf-8 -*-
import re

from sqlalchemy import func, tuple_

from tender.models import DBSession
from tender.models.package import Package
from tender.models.bo_q_item import BoQItem
from tender.models.section import Section
from tender.models.task import Task
from sqlalchemy import update, select


class ImportException(Exception):
    pass


def check_float_value(value, value_name):
    try:
        res = float(value)
    except ValueError:
        raise ImportException('Некорректно указана %s %s.' % (value_name, value))
    else:
        return res


class VORImport(object):
    def __init__(self, data_array):
        self.data = data_array
        self.package = Package()
        self.package_row = 0
        self.package_cost = 0
        self.section_costs = dict()
        DBSession.add(self.package)
        DBSession.flush()

    def section_bd_sum_update(self):
        query_select = select(
            func.sum(Task.indicator_0_sum), func.sum(Task.indicator_1_sum)
        ).where(
            Section.package_id == Task.package_id, Section.h_path.ancestor_of(Task.h_path))
        query_update = update(Section).values(
            {tuple_(Section.indicator_0_sum, Section.indicator_1_sum).self_group(): query_select}
        ).where(Section.package_id == self.package.id)
        DBSession.execute(query_update)

    def package_cost_update(self):
        query_select = select(
            func.sum(Section.indicator_0_sum) + func.sum(Section.indicator_1_sum)).where(
            Section.package_id == Package.id, Section.parent_id == None)
        query_update = update(Package).values({Package.cost.self_group(): query_select}).where(
            Package.id == self.package.id)
        DBSession.execute(query_update)

    def section_add(self, row, bo_q_item_id, h_path, parent):
        section_data = {
            'record_type': 'SECTION',
            'bo_q_item_id': bo_q_item_id.id,
            'package_id': self.package.id,
            'package_row': self.package_row,
            'comment': row.get('Комментарий', ''),
            'h_path': h_path,
            'parent_h_path': parent.h_path if parent else '',
            'parent_id': parent.id if parent else None,
        }
        section = Section(**section_data)
        DBSession.add(section)
        DBSession.flush()
        return section

    def task_add(self, row, bo_q_item_id, h_path, parent):
        material_price = check_float_value(row.get('Цена за материал (с НДС)'), 'Цена за материал')
        work_price = check_float_value(row.get('Цена за работу (с НДС)'), 'Цена за работу')
        volume = check_float_value(row.get('Объём'), 'Объём')
        material_price_sum = round(material_price * volume, 2)
        work_price_sum = round(work_price * volume, 2)
        task_data = {
            'record_type': 'TASK',
            'bo_q_item_id': bo_q_item_id.id,
            'package_id': self.package.id,
            'package_row': self.package_row,
            'comment': row.get('Комментарий'),
            'h_path': h_path,
            'parent_h_path': parent.h_path if parent else '',
            'section_id': parent.id if parent else None,
            'indicator_0': material_price,
            'indicator_1': work_price,
            'indicator_0_sum': material_price_sum,
            'indicator_1_sum': work_price_sum,
            'volume': volume,
        }
        task = Task(**task_data)
        DBSession.add(task)
        DBSession.flush()
        return task

    def data_import(self):
        try:
            for row in self.data:
                self.package_row += 1
                h_path = row.get('№ п/п', '')
                if not h_path:
                    continue
                if not re.fullmatch(r'(^\d+)|([\d\.]+\d)', h_path):
                    raise ImportException('Некорректный путь %s.' % h_path)
                parent = DBSession.query(Section).filter(
                    Section.h_path == func.subpath(h_path, 0, -1),
                    Section.package_id == self.package.id,
                ).first()
                bo_q_item = {
                    'code': row.get('Шифр', ''),
                    'title': row.get('Наименование работы', '')
                }
                bo_q_item_id = BoQItem(**bo_q_item)
                DBSession.add(bo_q_item_id)
                DBSession.flush()
                if row.get('Тип ресурса', '') in ['Секция', 'секция']:
                    section = self.section_add(row, bo_q_item_id, h_path, parent)
                    self.section_costs[section.id] = {
                        'parent_id': parent.id if parent else None,
                        'material_price_sum': 0,
                        'work_price_sum': 0,
                    }
                else:
                    self.task_add(row, bo_q_item_id, h_path, parent)
            self.section_bd_sum_update()
            self.package_cost_update()
        except ImportException as e:
            DBSession.rollback()
            raise ImportException(e.args)
        else:
            DBSession.commit()
        return True
