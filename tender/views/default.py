from pyexcel.exceptions import FileTypeNotSupported
from pyramid.view import view_config


from tender.views.VOR_import import VORImport, ImportException


@view_config(route_name='home', renderer='../templates/tender.jinja2')
def tender_view(request):
    return {'project': 'tender'}


@view_config(route_name='load_work_estimate', renderer='json',  request_method='POST')
def load_work_estimate(request):
    try:
        file_name_array = request.POST['work_estimate_file'].filename.split('.')
        request.POST['work_estimate_file'].filename = '%s.%s' % (file_name_array[0], file_name_array[-1])
        data = request.get_records(field_name='work_estimate_file', name_columns_by_row=0)
        VORImport(data).data_import()
    except ImportException as e:
        return dict(is_ok=0, message=e.args)
    except FileTypeNotSupported as e:
        return dict(is_ok=0, message=' Невозможно загрузить файл.')
    else:
        return dict(is_ok=1)
