# -*- coding: utf-8 -*-
from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from tender.models import initialize_sql


def main(global_config, **settings):
    with Configurator(settings=settings) as config:
        # db initialize
        config.scan('tender.models')
        engine = engine_from_config(settings, 'sqlalchemy.')
        initialize_sql(engine)
        # other statements here
        config.include('pyramid_jinja2')
        config.include('pyramid_excel')
        config.include('.routes')
        config.scan()
    app = config.make_wsgi_app()
    return app
