# -*- coding: utf-8 -*-
from sqlalchemy import Column, Numeric

from tender.models.base_model import BaseModel


class Package(BaseModel):
    __tablename__ = 'package'

    cost = Column(Numeric(41, 8))

    def __init__(self, cost=None):
        self.cost = cost
