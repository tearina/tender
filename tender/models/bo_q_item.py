# -*- coding: utf-8 -*-
from sqlalchemy import Column, VARCHAR

from tender.models.base_model import BaseModel


class BoQItem(BaseModel):
    __tablename__ = 'bo_q_item'

    code = Column(VARCHAR(800))
    title = Column(VARCHAR(800))

    def __init__(self, code, title):
        self.code = code
        self.title = title
