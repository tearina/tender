# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, func, DateTime

from tender.models.meta import Base


class BaseModel(Base):
    __abstract__ = True

    id = Column(Integer, nullable=False, unique=True, primary_key=True, autoincrement=True)
    created_on = Column(DateTime(timezone=True), default=func.now())
    updated_on = Column(DateTime(timezone=True), default=func.now(), onupdate=func.now())

    def __repr__(self):
        return "<{0.__class__.__name__}(id={0.id!r})>".format(self)
