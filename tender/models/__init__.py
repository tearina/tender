# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, func, DateTime
from sqlalchemy.orm import configure_mappers, scoped_session, sessionmaker

from tender.models.meta import Base
from . import bo_q_item
from . import package
from . import section
from . import task


configure_mappers()

DBSession = scoped_session(sessionmaker())


def initialize_sql(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
