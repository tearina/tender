# -*- coding: utf-8 -*-
from collections import Counter

from sqlalchemy import Column, VARCHAR, Integer, Numeric, ForeignKey
from sqlalchemy_utils import LtreeType, Ltree

from tender.models.base_model import BaseModel


class Task(BaseModel):
    __tablename__ = 'task'

    indicator_0 = Column(Numeric(42, 8))
    indicator_1 = Column(Numeric(42, 8))

    package_id = Column(Integer, ForeignKey('package.id', ondelete='CASCADE'), nullable=False, index=True)
    package_row = Column(Integer)

    section_id = Column(Integer, ForeignKey('section.id', ondelete='CASCADE'), nullable=False, index=True)

    volume = Column(Numeric(42,8))
    comment = Column(VARCHAR(500))
    record_type = Column(VARCHAR(7))
    level = Column(Integer)
    indicator_0_sum = Column(Numeric(42,8))
    indicator_1_sum = Column(Numeric(42,8))

    bo_q_item_id = Column(Integer, ForeignKey('bo_q_item.id', ondelete='CASCADE'), nullable=False, index=True)

    h_path = Column(LtreeType)
    parent_h_path = Column(LtreeType)

    def __init__(self,  indicator_0, indicator_1, volume, package_row, package_id, comment, record_type, bo_q_item_id, h_path, section_id, parent_h_path=None, indicator_0_sum=0, indicator_1_sum=0):
        self.indicator_0 = indicator_0
        self.indicator_1 = indicator_1
        self.volume = volume
        self.package_row = package_row
        self.package_id = package_id
        self.comment = comment
        self.record_type = record_type
        self.indicator_0_sum = indicator_0_sum
        self.indicator_1_sum = indicator_1_sum
        self.bo_q_item_id = bo_q_item_id
        self.h_path = Ltree(h_path)
        self.section_id = section_id
        collection = Counter(h_path)
        self.level = collection['.'] + 1
        if parent_h_path:
            self.parent_h_path = parent_h_path


