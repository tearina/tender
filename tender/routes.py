# -*- coding: utf-8 -*-
def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)

    config.add_route('home', '/')
    config.add_route('load_work_estimate', 'load_work_estimate')
