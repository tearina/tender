
$(document).on('submit', '#work_estimate_form', (function () {
    var work_estimate_form = $(this);
    work_estimate_form.find('.alert-danger').addClass('d-none').html('');
    var postData = new FormData(this);
    $.ajax({
        url: work_estimate_form.data('url'),
        type: 'POST',
        dataType: 'json',
        data: postData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
            if (data['is_ok']){
                $('.alert-success').removeClass('d-none');
                work_estimate_form.addClass('d-none');
            }
            else if (data['message']) {
                work_estimate_form.find('.alert-danger ').removeClass('d-none').html(data['message']);
            }
        }
    });
    return false;
}));