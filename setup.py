import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


requires = [
    'pyramid==1.10.4',
    'pyramid_beaker',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'sqlalchemy==1.4.31',
    'sqlalchemy-utils',
    'psycopg2',
    'alembic==1.13.1',
    'waitress',
    'zope.sqlalchemy',
    'pyramid-excel',
    'pyexcel-xls',
    'pyexcel-xlsx==0.6.0',
    'openpyxl==3.0.10',
]


setup(
    name='tender',
    version='0.0',
    description='tender',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='tearina',
    author_email='9zink9@gmail.com',
    url='',
    keywords='web wsgi pyramid sqlalchemy alembic',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'paste.app_factory': [
            'main = tender:main'
        ],
        'console_scripts': [
            'initialize_tender_db = tender.initialize_db:main'
        ],
    },
)
